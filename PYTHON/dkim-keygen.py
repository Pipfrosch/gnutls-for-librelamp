#!/usr/bin/env python
# -*- coding: utf-8 -*-

# (c)2018 Michael A. Peters, Pipfrosch Press
# Version 0.1 December 31, 2018
# FLOSS under terms of MIT
# https://opensource.org/licenses/MIT

import os, sys, getopt, re
import datetime
import socket
import binascii
import validators
import hashlib, ed25519
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.backends import default_backend

version = 'dkim-keygen v0.1 --- 2018 December 31'
usage = 'Usage: man 8 dkim-keygen'
# default null (None)
encodedPrivateKey = None
encodedPublicKey = None
recordOwner = None
# default empty lists
hashAlgorithms = []
targs = []
serviceType = []
# defaults explicitly set
domain = 'localhost.localdomain'
bits = 2048
pkeyCharsPerLine = 128
verbose = False
# computed defaults
now = datetime.datetime.utcnow()
directory = os.getcwd()
selector = 'default' + str(now.year)
if now.year > 2020:
  keytype = 'ed25519'
else:
  keytype = 'rsa'

longswitches = []
shortnoarg = []
shortwarg = []
# switches without arguments
longswitches.append("clean-domain")
longswitches.append("help")
longswitches.append("version")
longswitches.append("append-domain")
shortnoarg.append('a')
longswitches.append("testmode")
shortnoarg.append('t')
longswitches.append("nosubdomains")
shortnoarg.append('S')
longswitches.append("verbose")
shortnoarg.append('v')
longswitches.append("restricted")
shortnoarg.append('r')
# switches with arguments
longswitches.append("Directory=")
shortwarg.append('D')
longswitches.append("domain=")
shortwarg.append('d')
longswitches.append("keytype=")
shortwarg.append('k')
longswitches.append("bits=")
shortwarg.append('b')
longswitches.append("hash-algorithms=")
shortwarg.append('h')
longswitches.append("selector=")
shortwarg.append('s')
longswitches.append("public-key-linewidth=")

shortswitches = ''.join(shortnoarg) + ':'.join(shortwarg) + ':'

def verboseMessage(mess):
  if verbose == True:
    print mess

def cleanExit(mess):
  print mess
  sys.exit(0)

#non-fatal warning message
def warningMessage(mess):
  if verbose == True:
    sys.stderr.write(mess + "\n")

#fatal command error
def commandError(mess):
  sys.stderr.write(mess + "\n")
  sys.exit(2)

#fatal sytem error
def systemError(mess):
  sys.stderr.write(mess + "\n")
  sys.exit(1)

def ed25519KeyPair():
  global encodedPrivateKey
  global encodedPublicKey
  verboseMessage("Generating ed25519 key pair...")
  # 42 bytes is overkill but does not hurt
  seed = hashlib.sha256(os.urandom(42)).digest()
  signing_key = ed25519.SigningKey(seed)
  verifying_key = signing_key.get_verifying_key()
  pvtHexKey = signing_key.to_ascii(encoding="hex")
  pubHexKey = verifying_key.to_ascii(encoding="hex")
  asn1HexPvt = "302e020100300506032b657004220420" + pvtHexKey
  #asn1HexPub = "302A300506032B6570032100" + pubHexKey
  encodedPrivateKey = "-----BEGIN PRIVATE KEY-----\n" + asn1HexPvt.decode("hex").encode("base64").strip() + "\n-----END PRIVATE KEY-----\n"
  encodedPublicKey = pubHexKey.decode("hex").encode("base64").strip()

def rsaKeyPair(mybits=2048):
  global encodedPrivateKey
  global encodedPublicKey
  verboseMessage("Generating " + str(mybits) + "-bit RSA key pair...")
  key = rsa.generate_private_key(
    backend=default_backend(),
    public_exponent=65537,
    key_size=mybits
  )
  private_key = key.private_bytes(
    serialization.Encoding.PEM,
    serialization.PrivateFormat.PKCS8,
    serialization.NoEncryption()
  )
  public_key = key.public_key().public_bytes(
    serialization.Encoding.PEM,
    serialization.PublicFormat.SubjectPublicKeyInfo
  )
  encodedPrivateKey = private_key.strip() + "\n"
  encodedPublicKey = re.sub("-----[A-Z]+ PUBLIC KEY-----","",public_key)
  encodedPublicKey = re.sub("\n","",encodedPublicKey).strip()

# validates requested bits for RSA and generates private key
def validateBits():
  # 3072 and 4096 are here because some people have to use them due to bosses that are idiots.
  #  in reality, for signatures 2048 is perfectly secure, higher just wastes CPU cycles.
  arr = [ 2048, 3072, 4096 ]
  if keytype == 'ed25519':
    ed25519KeyPair()
  elif bits in arr:
    rsaKeyPair(bits)
  else:
    commandError("RSA key size must be 2048, 3072, or 4096. Other sizes are not supported. Program exiting now.")

def validateType():
  global keytype
  arr = [ "ed25519", "rsa" ]
  keytype = keytype.lower().strip()
  if keytype not in arr:
    commandError("Key type must be ed25519 or rsa. Program exiting now.")
  validateBits()

def validateSelector():
  global selector
  selector = selector.lower().strip()
  if len(selector) == 0:
    commandError("A selector can not be empty.")
  if selector.startswith('.') or selector.endswith('.'):
    commandError("A selector may not start or end with a period.")
  if selector.startswith('-') or selector.endswith('-'):
    commandError("A selector may not start or end with a dash.")
  if '..' in selector:
    commandError("A selector may not contain consecutive periods.")
  reg=re.compile('^[a-z0-9\._-]+$')
  if not reg.match(selector):
    commandError("A selector may only contain letters, numbers, dashes, underscores, and periods.")

def validateDirectory():
  if not os.path.isdir(directory):
    commandError("Specified directory does not exist.")
  if not os.access(directory, os.W_OK):
    commandError("Can not write to the directory " + directory)

def validateDomain():
  global domain
  domain = domain.strip()
  if not validators.domain(domain):
    systemError("The domain name " + domain + " is not a valid domain name. Exiting now.")
  try:
    socket.gethostbyname(domain)
  except:
    warningMessage("Warning: The specified domain name does not appear to resolve. Continuing anyway.")

def validateHashAlgo(string):
  global hashAlgorithms
  arr = [ 'sha256', 'sha384' ]
  inp = string.split(':')
  for i in range(len(inp)):
    algo = inp[i].lower().strip()
    algo = re.sub("sha-","sha",algo)
    if algo not in arr:
      commandError("SHA-256 and SHA-384 are the only supported hash algorithms. " + algo + " is not supported.")
    if algo not in hashAlgorithms:
      hashAlgorithms.append(algo)
  if "sha384" in hashAlgorithms:
    warningMessage("Warning: SHA-384 is not an RFC compliant hash algorithm. Some DKIM implementations do not support it.")
  if "sha256" not in hashAlgorithms:
    warningMessage("Warning: SHA-256 is not specified in your hash algorithms. This is likely a mistake.")
  hashAlgorithms.sort()

def writePrivateKey():
  os.umask(0o277)
  pvtfile = os.path.join(directory, selector) + '.private'
  if os.path.exists(pvtfile):
    systemError("The file " + pvtfile + " already exists. Exiting now.")
  try:
    file = open(pvtfile, "w")
  except:
    systemError('Failed to open ' + pvtfile + ' for writing.')
  file.write(encodedPrivateKey)
  file.close()
  verboseMessage('Private key written to file : ' + pvtfile)

def writePublicKey():
  if len(hashAlgorithms) == 0:
    halgo = "sha256"
  else:
    halgo = ':'.join(hashAlgorithms)
  os.umask(0o022)
  if pkeyCharsPerLine < 64:
    lw = 64
  elif pkeyCharsPerLine > 255:
    lw = 255
  else:
    lw = pkeyCharsPerLine
  dnsfile = os.path.join(directory, selector) + '.dns.txt'
  if len(targs) > 0:
    targstring="t=" + ':'.join(targs) + "; "
  else:
    targstring=''
  if len(serviceType) > 0:
    servicestring="s=" + ':'.join(serviceType) + "; "
  else:
    servicestring=''
  mylength = len(encodedPublicKey)
  cur = 0
  data = recordOwner + "\tIN\tTXT\t( \"v=DKIM1; h=" + halgo + "; k=" + keytype + "; " + targstring + servicestring + "p="
  while mylength > 0:
    if mylength < lw:
      data = data + "\"\n\t\"" + encodedPublicKey[cur:]
      mylength = 0
    else:
      end = cur + lw
      data = data + "\"\n\t\"" + encodedPublicKey[cur:end]
      cur = cur + lw
      mylength = mylength - lw
  data = data + "\" )"
  data = data + " ; ----- DKIM key " + selector + " for " + domain + "\n"
  try:
    file = open(dnsfile, "w")
  except:
    systemError('Failed to open ' + dnsfile + ' for writing.')
  file.write(data)
  file.close()
  verboseMessage('Public key written to file  : ' + dnsfile)

def main(argv):
  global bits
  global keytype
  global selector
  global directory
  global domain
  global recordOwner
  global targs
  global serviceType
  global verbose
  global pkeyCharsPerLine
  appendDomain = False
  cleanDomain = False
  try:
    opts, args = getopt.getopt(argv,shortswitches,longswitches)
  except getopt.GetoptError:
    commandError(usage)
  if "-v" in argv:
    verbose = True
  elif "--verbose" in argv:
    verbose = True
  for opt, arg in opts:
    if opt == "--help":
      cleanExit(usage)
    elif opt == "--clean-domain":
      cleanDomain = True
    elif opt == "--public-key-linewidth":
      pkeyCharsPerLine = int(arg)
    elif opt in ("-V", "--version"):
      cleanExit(version)
    elif opt in ("-a", "--append-domain"):
      appendDomain = True
    elif opt in ("-t", "--testmode"):
      if "y" not in targs:
        targs.append("y")
    elif opt in ("-S", "--nosubdomains"):
      if "s" not in targs:
        targs.append("s")
    elif opt in ("-r", "--restricted"):
      if "email" not in serviceType:
        serviceType.append("email")
    elif opt in ("-h", "--hash-algorithm"):
      validateHashAlgo(arg)
    elif opt in ("-D", "--Directory"):
      directory = arg
    elif opt in ("-d", "--domain"):
      tmp = unicode(arg, encoding=sys.stdin.encoding)
      domain = tmp.encode('idna')
      validateDomain()
    elif opt in ("-k", "--keytype"):
      keytype = arg
    elif opt in ("-b", "--bits"):
      bits = int(arg)
    elif opt in ("-s", "--selector"):
      selector = arg
      validateSelector()
  if cleanDomain == True:
    cleanExit(domain)
  if appendDomain == True:
    recordOwner = selector + "._domainkey." + domain + "."
  else:
    recordOwner = selector + "._domainkey"
  # validate other inputs
  validateDirectory()
  validateType()
  writePrivateKey()
  writePublicKey()

if __name__ == "__main__":
  main(sys.argv[1:])
