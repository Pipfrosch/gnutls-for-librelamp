.TH dkim\-keygen 8 "December 31, 2018" "version 0.1" "SYSTEM ADMINISTRATION COMMANDS"
.SH NAME
dkim\-keygen \- generate RSA or Ed25519 key pairs for DKIM email message signing
.SH SYNOPSIS
.B dkim\-keygen [options]
.SH DESCRIPTION
DomainKeys Identified Mail (DKIM) uses public / private key cryptography to
sign email as they pass through a server authorized to relay mail on behalf of
the sending mailbox domain. The public key associated with the message signing
key is stored in the DNS system. This allows receiving MX servers and email
clients to validate the message went through a server authorized to relay
email messages on behalf of the mailbox domain (when used in combination with
SPF) and allows MX servers and email clients to verify that email message
has not likely been altered since it was signed.
.PP
This programs generates RSA or Ed25519 key pairs suitable for DKIM message
signing as well as a sample BIND compatible DNS record that can be used to
distribute the public key using the DNS system.
.PP
In addition to RFC 6376 (September 2011), dkim\-keygen implements the tighter
requirements regarding RSA key size and hashing algorithm from update RFC 8301
(January 2018) and supports the Ed25519 key type from update RFC 8463 (September
2018).
.PP
Options are largely based on the opendkim\-genkey program from the OpenDKIM
projects with a few differences.
.PP
If you find this program useful, the author suffers from both medical and
social disabilities that have resulted in extreme poverty. Donations can be
made at
.B https://librelamp.com/Donate
.SH FREQUENTLY USED OPTIONS
These are the most frequently used options.
.TP
\-d domain
[
.B \-\-domain=string
] Specify the mailbox domain the key pair is to be used with.
This switch only has a functional value when the \-a (\-\-append\-domain) switch
is also used. Otherwise it only impacts a DNS comment that is not part of the
DNS record sent to clients. The default value is localhost.localdomain.
.TP
\-D directory
[
.B \-\-directory=path
] Specify the directory where the private key and the DNS text
record containing the public key are to be written. The default is the current
working directory dkim\-keygen is called from.
.TP
\-k keytype
[
.B \-\-keytype=name
] Specify whether you want an RSA key pair or an Ed25519 key
pair. If your system clock reports the year as 2020 (UTC) or earlier, the
default key type is RSA. Starting in 2021 (UTC) the default key type changes to
Ed25519.
.TP
\-s selector
[
.B \-\-selector=name
] Specify the selector to be used (see RFC 6376) with the key
pair. The default value is the word "default" appended with the current year as
reported by your system clock (in UTC).
.SH OTHER OPTIONS
These options are rarely actually needed.
.TP
\-a
[
.B \-\-append\-domain
] In most cases, it is fine to leave the owner of the DNS
record as the selector followed by the string ._domainkey and the zone file will
do the right thing. In some cases, you may need to append the mailbox domain to
the end of the owner field to create a fully qualified domain owner ending with
a dot. This option does that for you.
.TP
\-b bits
[
.B \-\-bits=n
] Only meaningful for RSA key pairs. This option allows you to
specify the size in bits of the RSA key pair. The default value is 2048, which
is the recommended size from RFC 8301 (January 2018). Other allowed values are
3072 and 4096. 1024 bit key pairs are not supported by dkim\-keygen even though
they are technically still RFC compliant.
.TP
\-\-clean\-domain
Output the domain name as dkim\-keygen sees it and exit. No key pair will be
generated. This is useful for seeing how the dkim\-keygen program will convert
a non-ASCII domain name to IDNA ASCII equivalent.
.TP
\-h algorithm
[
.B \-\-hash\-algorithms=name[:name[...]]
] Specify one or more colon delimited hash
digest algorithms that may be used to sign a message. RFC 6376 (September 2011)
allows sha1 and sha256 with RSA keys, but recommends using sha256. RFC 8301
(January 2018) deprecates the use of sha1 with RSA and RFC 8463 (September
2018) only allows sha256 with Ed25519. This program rejects sha1 as a valid
hash algorithm, but allows specifying sha384 even though it is not (yet) an RFC
condoned algorithm. The default value is sha256. Other legal values are sha384
and sha256:sha384.
.TP
\-\-help
Print a message instructing the user to read this man page.
.TP
\-\-public\-key\-linewidth=n
BIND limits strings in the RDATA field to a maximum of 255 (ASCII) characters.
Even with base64 encoding, the smallest RSA key sizes that are safe to use with
DKIM exceed that limit and must be broken into multiple separate strings. For
human readability, even 255 characters is too long because most console window
width are smaller causing the string to wrap. This switch allows you to define
what line width you wish to use when generating the public key. The default is
128 characters. Arguments smaller than 64 are interpreted as 64. Arguments
above 255 are interpreted as 255.
.TP
\-r
[
.B \-\-restricted
] Specify that the DKIM public key is only valid in the context
of email. This option only exists because RFC 6376 (September 2011) specifies a
flag for the DNS RDATA field in the event that DKIM records are ever used for
more than just email. In reality, no other service is likely to ever use Domain
Keys for public key distribution, DANE is better suited for most use cases where
public keys need DNS distribution or validation.
.TP
\-S
[
.B \-\-nosubdomains
] Specify that only the mailbox domain may use the signing key
for signing messages. The default is to allow subdomains to sign messages with
it as well.
.TP
\-t
[
.B \-\-testmode
] Specifies that when DKIM validating an email message against the
the signature, the message should be interpreted and processed as if no
signature was present. Validation or failure of the signature should not have
a bearing on how the message is treated, DKIM is in test mode for that
selector.
.TP
\-v
[
.B \-\-verbose
] Ordinarily only program terminating error messages are output to
the console. This switch results in some additional verbosity.
.TP
\-V
[
.B \-\-version
] Output the version of the program and exit.
.SH RFC NOTES
.TP
RFC 6376
(September 2011) Defines RSA for message signing. Defines SHA-1 and SHA-256 for
signature hash algorithm. Defines 512 bits to 2048 bits as the RSA key size range
that verifiers
.B MUST
be able to validate. Defines the RDATA parameters for the DNS TXT record. Public
key is base64 encoded with ASN.1 serialization bits.
.B https://tools.ietf.org/html/rfc6376
.TP
RFC 8301
(January 2018) Deprecates SHA-1 hash algorithm, specifies that signers
.B MUST
use SHA-256. Deprecates 512 bit RSA key pairs, signers
.B MUST
use key sizes of at least 1024 bits and
.B SHOULD
use RSA keys of at least 2048 bits. The RFC does not specify an upper bound for
RSA key size, but in practice 4096 bits is excessive.
.B https://tools.ietf.org/html/rfc8301
.TP
RFC 8463
(September 2018) Defines Ed25519 for message signing. Example RDATA use a
base64 encoded public key without ASN.1 serialization bits. They really are not
needed with Ed25519.
.B https://tools.ietf.org/html/rfc8463
.SH USAGE NOTES
A PEM file will be created for the private signing key using the name of the
selector followed by the extension .private within the specified directory. If a
file already exists with that name, the program will instead exit. The file
containing the private is generated as only readable by the user that invoked
the dkim\-keygen program.
.PP
A text file containing a BIND format DNS record including a base64 encoding of
the public key will be created using the name of the selector followed by the
extension .dns.txt within the specified directory. If a file already exists with
that name, it will be overwritten. The file containing the DNS record is
generated with world read permissions.
.SH BEST PRACTICES
A fresh key pair should be generated at least once a year. The new key pair
should have a different selector so that the public key with the old selector
can remain in DNS for a few weeks after the signing switch, allowing email
clients to continue validating signatures using the old key. The old selector
.B SHOULD
be removed from DNS a few weeks after the change.
.PP
Ed25519 as a valid signature algorithm is too new to use in production. Even if
your DKIM implementation supports Ed25519, many MX servers and e-mail clients do
not. I recommend continuing to use RSA key pairs until 2021.
.PP
Do not use RSA keys smaller than 2048 bits. Just do not. It is probably safe
for signing purposes if frequently rotated, but real world DKIM usage shows that
the key pairs are rarely rotated. This lack of rotation is likely due to the
lack of certificates that expire and due to the required updates to the DNS 
zone file. Administrators get lazy.
.PP
Do not use RSA keys larger than 2048 bits unless it is mandated by your
employer. The computational work required to sign each message grows
exponentially as RSA key size grows. DKIM does not guarantee the alleged sender
sent the message, that can only be done with PGP or S/MIME signatures. For the
purpose of ephemeral message signing that only verifies it passed through a
particular server, the key size only needs to be large enough that it is not
conveivable the key will be brute forced before it is taken out of service.
.PP
Given that it is not likely 2048 bit RSA will be vulnerable to brute force
cracking within the next decade, it is a mis-use of computational resources to
use RSA keys larger than 2048 bit for DKIM message signing.
.PP
While not required by an RFC, you should implement DNSSEC. DKIM public keys are
not signed and therefore can not be validated through the PKI system. DNSSEC is
the only mechanism by which the authenticity of the public key can be trusted.
.SH PYTHON DEPENDENCIES
dkim\-keygen uses python\-ed25519 to generate the Ed25519 key pair. You can obtain
that dependency from
.B https://github.com/warner/python\-ed25519
.PP
dkim\-keygen uses python\-validators to validate ASCII domain names. You can
obtain that dependency from
.B https://github.com/kvesteri/validators
.PP
dkim\-keygen uses python\-cryptography for RSA related functions. You can usually
obtain that dependency from your operating system vendor or Python distribution
vendor.
.PP
dkim\-keygen uses the Python os, sys, getopt, re, datetime, socket, binascii,
and hashlib packages. These are ordinarily installed with Python but should be
available from your operating system vendor or Python distribution vendor if
you are missing any of them.
.SH EXAMPLES
.TP
Create an Ed25519 key pair for example.org with the selector butterfly:
.B dkim\-keygen \-k ed25519 \-d example.org \-s butterfly
.TP
Create a 3072 bit RSA key using the default selector, allow SHA-384 hash algorithm in verbose mode:
.B dkim\-keygen \-k rsa \-b 3072 \-h sha256:sha384 -v
.SH EXIT STATUS
dkim\-keygen returns an exit status of zero on successful invocation. It returns
an exit status of one if the program fails. It returns an exit status of two
with a command syntax failure.
.SH BUGS
The python\-ed25519 dependency allegedly does not work in current versions of
Python 3.6. This failure needs to be validated, investigated, and fixed.
.SH SECURITY CONSIDERATIONS
The private key should be installed such that it is only readable by the root
user and the daemon that uses it to sign messages. When created, the private
key can only be read by the user that called the dkim\-keygen program.
.SH WILL NOT IMPLEMENT
The dkim\-keygen program is intended as a replacement for the opendkim\-genkey
program. There are some options from that program that will not be supported.
.TP
\-n note
[
.B \-\-note=string
] In opendkim\-genkey this option is used to specify an arbitrary
note with the DNS record. RFC 6376 (September 2011) specifies such a note as
allowed but also specifies it is to be ignored by programs fetching the record.
The implied purpose is for system administrators to leave a note either to
themselves or to another system administrator. It is my opinion that the RDATA
field of a DNS record is the wrong place for such notes.
.TP
\-\-subdomains
In opendkim\-genkey this option is used to specify that the key
pair may be used for sub\-domains. This is already the default behavior.
.TP
\-\-notestmode
In opendkin\-genkey this option specifies that the key pairs are not being in
test mode. This is already the default behavior.
.SH AUTHOR
Alice Wonder (alice (at) librelamp (dot) com)
.SH LICENSE
dkim\-keygen and this man page are released under the terms of the
MIT License. See
.B https://opensource.org/licenses/MIT
.SH SEE ALSO
opendkim\-genkey(8)
