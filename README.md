GnuTLS For LibreLAMP
====================

LibreLAMP is an alternate LAMP stack for CentOS 7 that I maintain, built upon
LibreSSL as an alternate to OpenSSL. It has become clear to me that some
software is better suited to GnuTLS API than OpenSSL API.

This project will be a repository for RPM spec files and patches related to the
GnuTLS 3.6.x packages that I build for LibreLAMP.

Install Prefix
--------------

Where packages for GnuTLS 3.6.x API conflict with packages from CentOS 7 / EPEL
an install prefix of `/opt/gnutls` will be used. The package `gnutls-slashopt`
owns the directory structure and the ldconfig configuration file needed to
load the dynamic libraries without requiring an rpath.

Packages that do not cause a conflict problem with CentOS 7 / EPEL continue to
use `/usr` as the install prefix.

### `PKG_CONFIG_PATH`

When building against libraries that use the `/opt/gnutls` prefix, you will
need to

    export PKG_CONFIG_PATH="/opt/gnutls/lib64/pkgconfig"

before running configure so that the correct `pkgconfig` files will be picked
up.

### `man` and `info` files

For packages that use `/opt/gnutls` as an install prefix, I do not currently
package `man` or `info` files. That may come in the future, if the respective
paths can be properly be added. I honestly have not looked into it.

I need to avoid the man and info files conflicting with the CentOS 7 / EPEL
versions.

GnuTLS Build Dependencies
-------------------------

This section is to document the build dependencies external to CentOS 7 / EPEL.

### `p11-kit-devel`

The version of `p11-kit` that ships with CentOS 7 / EPEL appears to be too old
to build against. An updated version of that package from Fedora works and does
not version the shared library, so it is built with an install prefix of `/usr`
and does not appear to result in any conflicts with CentOS 7 / EPEL packages
that build against it.

No patches are currently applied.

### `compat-nettle-devel`

CentOS 7.6 ships with nettle 2.7.1 which is not new enough to build GnUTLS
3.6.x branch.

While the shared library *could* be installed in `/usr/lib64` without a file
conflict, the binaries can not be installed in `/usr/bin` without a file
conflict so the package is built with a prefix of `/opt/gnutls`.

No patches are currently applied.

The SPEC file needs to be updated to validate against a `sha256sum` or
preferably a signature for the source tarball.

### `datefudge`

To pass tests on a 64-bit platform, `datefudge` needs to be at version 1.22 or
newer. A proper version is currently in EPEL testing and works.

### `unbound-devel`

In LibreLAMP a newer version of unbound is used than what ships with CentOS /
EPEL 7. That newer unbound is what the GnuTLS 3.6.x branch builds against.

However the RPM spec file is NOT in this project because I do not believe the
newer unbound is a *requirement* to build GnuTLS 3.6.x.

GnuTLS Patches
--------------

These two patches are inherited from the Fedora `src.rpm` I started with:

* gnutls-3.2.7-rpath.patch
* gnutls-3.6.4-no-now-guile.patch

These next two patches disable tests related to PKCS11. I *believe* the actual
cause of the test failure is an outdated `softhsm` package in CentOS 7 but I
have not verified.

I believe those test failures are only indicative of `softhsm` version and are
not indicative of bugs.

* gnutls-3.6.5-disabletests-tls-neg-pkcs11-key.c.patch
* gnutls-3.6.5-disabletests-testpkcs11.sh.patch

The final patch deals with building the `cipher-openssl-compat` test program
against LibreSSL as opposed to building the same test program against OpenSSL.

It is taken from [GnuTLS Merge Request 846](https://gitlab.com/gnutls/gnutls/merge_requests/846)
and will likely not be needed with GnuTLS 3.6.6.

* gnutls-3.6.5-libressl.patch

GnuTLS Spec File
----------------

The RPM spec file is called `compat-gnutls.spec` and builds packages with the
`compat-` prefix so that it does not conflict with the GnuTLS packages for the
older version of GnuTLS that some CentOS 7 / EPEL 7 packages expect.

Like the Nettle package, it uses `/opt/gnutls` as the install prefix.

OpenDKIM
--------

OpenDKIM is why I initially started playing with the GnuTLS 3.6.x branch.

The version of OpenDKIM in EPEL for CentOS 7 will likely *never* support the
[RFC 8463](https://tools.ietf.org/html/rfc8463) specified Ed25519-sha256
signature algorithm because neither OpenSSL or GnuTLS in CentOS 7 will be
updated to support Ed25519.

It is quite probable that a future version of the LibreSSL package that I
maintain *will* support Ed25519 once TLS 1.3 support has been added, but that
support is currently not there and OpenDKIM looks needs patching to work with
LibreSSL. The project is committed to GnuTLS support, it just seems much more
logical to use GnuTLS for OpenDKIM.

Presently the GnuTLS support for Ed25519-sha256 signatures does not seem to
*actually work* but RSA-sha256 signing and validation does work, so I do not
lose any functionallity by switching to GnuTLS for the OpenDKIM signing and
verification library now. Honestly I think libsodium would be an even better
choice since DKIM does not make a TLS connection but OpenDKIM project seems to
prefer using a TLS library, maybe because the original DKIM specification uses
Base64 encoded DER for the public key.

Personally I would like to see a DKIM version 2 that *requiures* DNSSEC
validation of the DNSSEC record (similar to the TLSA record with DANE) and only
includes a base64 encoding of the public key, since the algorithm is already
specified separately. Also, only ECDSA and EdDSA signatures should be used in
a DKIM version 2. But anyway that is going off topic.

The OpenDKIM packages *replaces* the EPEL 7 version of the package.

### Known Issues

With the private key, when there is any white space above or below the PEM
`-----{BEGIN,END} PRIVATE KEY-----` for Ed25519 private keys, the private key
is rejected as invalid. I believe this a bug in OpenDKIM and not a bug in
GnuTLS as `certtool` has no issues working with the files.

With the private key, when there is NOT any white space above or below the PEM
`-----{BEGIN,END} PRIVATE KEY-----` for Ed25519 private keys, OpenDKIM accepts
it as a valid private key but the daemon crashes. Again I believe this is a bug
in OpenDKIM rather than in GnuTLS.

With RSA private keys, OpenDKIM works as expected with a GnuTLS backend. At
least with 2048-bit RSA, I have not tried larger or smaller. Smaller than 2048
is no longer recommended as of [RFC 8301](https://tools.ietf.org/html/rfc8301)
and larger than 2048-bit RSA is a waste of computing resources for digital
signatures where nothing is encrypted with the key.

### Patches

The OpenDKIM package as I build it has one patch applied.

* OpenDKIM-rel-opendkim-2-11-0-Beta2-gnutls-ed25519-verify.patch

The above patch is *suppose* to allow OpenDKIM to verify Ed25519-sha256
signatures when a GnuTLS backend is used. I do not know if that actually works
yet as I have not been successful in getting Ed25519-sha256 signatures to work.

That patch (or one similar) is expected to be part of OpenDKIM 2.11.0 when it
reaches general release maturity.

### `dkim-keygen`

In the `PYTHON` directory is a script for generating both RSA and Ed25519 key
pairs for use with DKIM. This python script is intended as a replacement for
the `opendkim-genkey` script that is part of the OpenDKIM project. A `man`
page is also in that directory.

The Python script currently uses `Python-ed25519` which appears to be an
abandoned project that does not work with current versions of Python3 so I will
likely update the script to use a different Python library for the Ed25519
support.

### `opendkim-default-keygen`

As packaged in EPEL, OpenDKIM includes a shell script that takes no arguments
and generates default key pairs for DKIM. Unfortunately it does not do a very
good job at detectig the mailbox domain (it makes no attempt to query postfix)
and it uses `opendkim-genkey` resulting in 1024-bit RSA keys.

In the `BASH` directory is a script called `librelamp-default-dkimkeygen.sh`
which I install as `opendkim-default-keygen` in my packaging of OpenDKIM.
It uses `dkim-keygen` resulting in a safer key pair as well as better detection
of the mailbox domain. A man page is also in that directory, where it is
called `opendkim-default-keygen.8` as that matches the command name when the
RPM installs the script.

### Default Configuration Files

In the `CONFIG` directory are default configuration files packaged with the
OpenDKIM package. These differ from the default configuration files that are
packaged with the EPEL package.

Wget
----

LibreLAMP has packaged an updated version of `wget` for some time. Now that
GnuTLS 3.6.x is in LibreLAMP it is being built against that version, giving
wget TLS 1.3 support.

### `compat-libpsl-devel`

Current versions of `wget` require a newer `libpsl` library than what ships
with CentOS 7. The updated library can be installed in `/usr` in parallel with
the CentOS version of the library, so that is what I do.

### Patches

The following three patches are applied to the source:

* `wget-1.20-librelamp-modified.patch`
* `wget-1.20-path.patch`
* `wget-1.18-fix-double-free-of-iri-orig_url.patch`

The first patch simply changes the bug report string. The second two are
from the Fedora packaging of `wget`.

Wget2
-----

This is a work in progress spec file for Wget2. The `gnulib-devel` package is
a rebuild of what is in current Fedora. `make check` is currently broken.

A newer `gnulib` would likely resolve some build issues including the broken
`make check` but I am currently having trouble getting the newer `gnulib` to
build in CentOS 7.

The spec file for `wget2` builds, but the result should be considered
experimental and not production ready.

FUTURE
------

I would like to build the Apache `mod_gnutls` module and do some benchmark
tests comparing it against `mod_ssl` that is built against LibreSSL.

I will wait with that until a new version of `mod_gnutls` has been released.

I am also considering packaging Exim built against GnuTLS 3.6.x but I might not
do that.

There also is a GnuTLS Python module that may be of use to some people if it
is built against 3.6.x branch of GnuTLS.
