Name:		gnutls-slashopt
Version:	1
Release:	1%{?dist}
Summary:	filesystem support for updated gnutls installed in /opt

Group:		System Environment/Base
License:	Not Applicable
URL:		https://librelamp.com/

%description
This package provides the /opt/gnutls prefix and the ldconfig config file
needed to install an updated gnutls and dependencies within /opt where
they will not conflict with the CentOS versions

%prep

%build

%install
mkdir -p %{buildroot}/opt/gnutls/bin
mkdir -p %{buildroot}/opt/gnutls/%{_lib}/pkgconfig
mkdir -p %{buildroot}/opt/gnutls/include
mkdir -p %{buildroot}/opt/gnutls/share/doc
mkdir -p %{buildroot}/etc/ld.so.conf.d

cat <<EOF > %{buildroot}/etc/ld.so.conf.d/gnutls.conf
# For updated gnutls libraries
/opt/gnutls/%{_lib}
EOF


%files
%defattr(-,root,root,-)
%dir /opt/gnutls
%dir /opt/gnutls/bin
%dir /opt/gnutls/%{_lib}
%dir /opt/gnutls/%{_lib}/pkgconfig
%dir /opt/gnutls/share/doc
%config %attr(0644,root,root) /etc/ld.so.conf.d/gnutls.conf



%changelog
* Mon Dec 10 2018 Alice Wonder <buildmaster@librelamp.com> - 1-1
- Initial package
