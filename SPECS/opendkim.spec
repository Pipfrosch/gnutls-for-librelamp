Name:		opendkim
Version:	2.11.0
Release:	0.24.Beta2%{?dist}.1
Summary:	A DomainKeys Identified Mail (DKIM) milter to sign and/or verify mail

Group:		System Environment/Daemons
License:	BSD and Sendmail
URL:		http://www.opendkim.org/
Source0:	OpenDKIM-rel-opendkim-2-11-0-Beta2.tar.gz
Source1:	opendkim.README.librelamp
#configuration files
Source10:	opendkim.conf.librelamp
Source11:	opendkim.SigningTable.librelamp
#custom programs
Source20:	dkim-keygen.py
Source21:	dkim-keygen.8
Source22:       librelamp-default-dkimkeygen.sh
Source23:       opendkim-default-keygen.8

# Patches I expect in upstream
Patch1:		OpenDKIM-rel-opendkim-2-11-0-Beta2-gnutls-ed25519-verify.patch

# Patches that may not make upstream
#Patch51:	OpenDKIM-rel-opendkim-2-11-0-Beta2-RSA2048-default.patch

BuildRequires:	libtool, pkgconfig, libbsd, libbsd-devel, opendbx-devel
BuildRequires:  automake, autoconf
BuildRequires:	compat-gnutls-devel
BuildRequires:	libdb-devel, libmemcached-devel
BuildRequires:	sendmail-devel
BuildRequires:	openldap-devel
Requires:	libopendkim%{?_isa} = %{version}-%{release}
Requires:	python-ed25519 python-validators python2-cryptography
Requires(pre):	shadow-utils
Requires(post):	policycoreutils

%description
OpenDKIM allows signing and/or verification of email through an open source
library that implements the DKIM service, plus a milter-based filter
application that can plug in to any milter-aware MTA, including sendmail,
Postfix, or any other MTA that supports the milter protocol.

%package -n libopendkim
Summary:	An open source DKIM library
Group:		System Environment/Libraries

%description -n libopendkim
This package contains the shared library files needed for running applications
linked against the libopendkim shared libraries.

%package -n libopendkim-devel
Summary:	Development files for libopendkim
Group:		Development/Libraries
Requires:	libopendkim%{?_isa} = %{version}-%{release}
Requires:	compat-gnutls-devel

%description -n libopendkim-devel
This package contains the header files and needed for building software that
links against the libopendkim libraries.


%prep
%setup -q -n OpenDKIM-rel-opendkim-2-11-0-Beta2
%patch1 -p1
#%%patch51 -p1


%build
%define LIBTOOL LIBTOOL=`which libtool`
autoreconf -i
export PKG_CONFIG_PATH="/opt/gnutls/%{_lib}/pkgconfig"

%configure --with-gnutls=auto --with-odbx --with-db --with-libmemcached --with-openldap --enable-query_cache

make %{?_smp_mflags}
cp %{SOURCE1} ./LIBRELAMP.README

%install
make install DESTDIR=%{buildroot}

install -d %{buildroot}%{_sysconfdir}
install -d %{buildroot}%{_sysconfdir}/sysconfig

#systemd
install -d -m 0755 %{buildroot}%{_unitdir}
install -m 0644 contrib/systemd/opendkim.service %{buildroot}%{_unitdir}/opendkim.service

#configuration files
install -m644 %{SOURCE10} %{buildroot}%{_sysconfdir}/opendkim.conf
cat <<EOF > %{buildroot}%{_sysconfdir}/sysconfig/opendkim
# Set the necessary startup options
OPTIONS="-x %{_sysconfdir}/opendkim.conf -P %{_localstatedir}/run/opendkim/opendkim.pid"

# Set the default DKIM selector
DKIM_SELECTOR=default2019

# Set the default DKIM key location
DKIM_KEYDIR=%{_sysconfdir}/opendkim/keys
EOF
mkdir -p %{buildroot}%{_sysconfdir}/opendkim
install -m640 %{SOURCE11} %{buildroot}%{_sysconfdir}/opendkim/SigningTable
cat <<EOF > %{buildroot}%{_sysconfdir}/opendkim/KeyTable
# OPENDKIM KEY TABLE
# To use this file, uncomment the #KeyTable option in /etc/opendkim.conf,
# then uncomment the following line and replace example.com with your domain
# name, then restart OpenDKIM. Additional keys may be added on separate lines.

#default2019._domainkey.example.com example.com:default:/etc/opendkim/keys/default.private
EOF
cat <<EOF > %{buildroot}%{_sysconfdir}/opendkim/TrustedHosts
# OPENDKIM TRUSTED HOSTS
# To use this file, uncomment the #ExternalIgnoreList and/or the #InternalHosts
# option in /etc/opendkim.conf then restart OpenDKIM. Additional hosts
# may be added on separate lines (IP addresses, hostnames, or CIDR ranges).
# The localhost IP (127.0.0.1) should always be the first entry in this file.
127.0.0.1
::1
#host.example.com
#192.168.1.0/24
EOF
mkdir -p %{buildroot}%{_sysconfdir}/tmpfiles.d
cat <<EOF > %{buildroot}%{_sysconfdir}/tmpfiles.d/opendkim.conf
D %{_localstatedir}/run/opendkim 0700 opendkim opendkim -
EOF

#support directories
mkdir -p %{buildroot}%{_localstatedir}/spool/opendkim
mkdir -p %{buildroot}%{_localstatedir}/run/opendkim
mkdir -p %{buildroot}%{_sysconfdir}/opendkim
mkdir %{buildroot}%{_sysconfdir}/opendkim/keys

install -m 0755 stats/%{name}-reportstats %{buildroot}%{_sbindir}/opendkim-reportstats
sed -i 's|OPENDKIMSTATSDIR="/var/db/opendkim"|OPENDKIMSTATSDIR="%{_localstatedir}/spool/opendkim"|g' %{buildroot}%{_sbindir}/opendkim-reportstats
sed -i 's|OPENDKIMDATOWNER="mailnull:mailnull"|OPENDKIMDATOWNER="opendkim:opendkim"|g' %{buildroot}%{_sbindir}/opendkim-reportstats

install -m755 %{SOURCE20} %{buildroot}%{_sbindir}/dkim-keygen
install -m644 %{SOURCE21} %{buildroot}%{_mandir}/man8/dkim-keygen.8
install -m 0755 %{SOURCE22} %{buildroot}%{_sbindir}/opendkim-default-keygen
install -m 0644 %{SOURCE23} %{buildroot}%{_mandir}/man8/opendkim-default-keygen.8

#nuke
rm -r %{buildroot}%{_prefix}/share/doc/%{name}
rm %{buildroot}%{_libdir}/*.a
rm %{buildroot}%{_libdir}/*.la

%check
make check

%pre
getent group opendkim >/dev/null || groupadd -r opendkim
getent passwd opendkim >/dev/null || \
        useradd -r -g opendkim -G mail -d %{_localstatedir}/run/opendkim -s /sbin/nologin \
        -c "OpenDKIM Milter" opendkim
exit 0

%post
%systemd_post %{name}.service

%preun
%systemd_preun %{name}.service

%postun
%systemd_postun_with_restart %{name}.service

%post -n libopendkim -p /sbin/ldconfig

%postun -n libopendkim -p /sbin/ldconfig

%clean
rm -rf %{buildroot}


%files
%defattr(-,root,root,-)
%doc LIBRELAMP.README RELEASE_NOTES RELEASE_NOTES.Sendmail KNOWNBUGS
%doc contrib/convert/convert_keylist.sh opendkim/*.sample
%doc opendkim/opendkim.conf.simple-verify opendkim/opendkim.conf.simple
%doc opendkim/README opendkim/README.SQL contrib/lua/*.lua
%license LICENSE LICENSE.Sendmail
%attr(0644,root,root) %{_unitdir}/opendkim.service
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/opendkim.conf
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/sysconfig/opendkim
%attr(0644,root,root) %config(noreplace) %{_sysconfdir}/tmpfiles.d/opendkim.conf
%dir %{_sysconfdir}/opendkim
%attr(0640,opendkim,opendkim) %config(noreplace) %{_sysconfdir}/opendkim/SigningTable
%attr(0640,opendkim,opendkim) %config(noreplace) %{_sysconfdir}/opendkim/KeyTable
%attr(0640,opendkim,opendkim) %config(noreplace) %{_sysconfdir}/opendkim/TrustedHosts
%dir %attr(-,opendkim,opendkim) %{_localstatedir}/spool/opendkim
%dir %attr(0755,opendkim,opendkim) %{_localstatedir}/run/opendkim
%dir %attr(0750,opendkim,opendkim) %{_sysconfdir}/opendkim/keys
%{_sbindir}/opendkim
%{_sbindir}/opendkim-genkey
%{_sbindir}/opendkim-genzone
%{_sbindir}/opendkim-testkey
%{_sbindir}/opendkim-testmsg
%{_sbindir}/opendkim-default-keygen
%{_sbindir}/opendkim-reportstats
%{_sbindir}/dkim-keygen
%{_mandir}/man5/opendkim.conf.5*
%{_mandir}/man8/opendkim-genkey.8*
%{_mandir}/man8/opendkim-genzone.8*
%{_mandir}/man8/opendkim-testkey.8*
%{_mandir}/man8/opendkim-testmsg.8*
%{_mandir}/man8/opendkim.8*
%{_mandir}/man8/dkim-keygen.8*
%{_mandir}/man8/opendkim-default-keygen.8*

%files -n libopendkim
%doc README README.ERLANG contrib/ldap/README.LDAP opendkim/README.SQL
%doc RELEASE_NOTES RELEASE_NOTES.Sendmail KNOWNBUGS
%license LICENSE LICENSE.Sendmail
%defattr(-,root,root,-)
%{_libdir}/libopendkim.so.*

%files -n libopendkim-devel
%doc README README.ERLANG RELEASE_NOTES RELEASE_NOTES.Sendmail FEATURES
%doc libopendkim/docs/*.html
%license LICENSE LICENSE.Sendmail
%defattr(-,root,root,-)
%{_includedir}/opendkim
%{_libdir}/pkgconfig/*.pc
%{_libdir}/libopendkim.so





%changelog
* Mon Dec 31 2018 Alice Wonder <buildmaster@librelamp.com> - 2.11.0-0.24.Beta2.1
- Replace opendkim-ed25519-keygen with dkim-keygen
- Replace upstream opendkim-default-keygen with custom version
- Do not apply patch 51

* Sat Dec 29 2018 Alice Wonder <buildmaster@librelamp.com> - 2.11.0-0.23.Beta2.2
- Update comment in configuration related to default signature algorithm.

* Wed Dec 19 2018 Alice Wonder <buildmaster@librelamp.com> - 2.11.0-0.23.Beta2.1
- Add opendkim-ed25519-keygen python program

* Tue Dec 11 2018 Alice Wonder <buildmaster@librelamp.com> - 2.11.0-0.22.Beta2.0
- Build against GnuTLS, now has ed25519 support

* Fri Dec 07 2018 Alice Wonder <buildmaster@librelamp.com> - 2.11.0-0.21.Beta2.0
- rebuild with properly named spec file

* Sun Nov 25 2018 Alice Wonder <buildmaster@librelamp.com> - 2.11.0-0.20.Beta2.1
- patch opendkim-genkey to warn when RSA less than 2048 requested and to do
- prettier printing of public key

* Fri Nov 23 2018 Alice Wonder <buildmaster@librelamp.com> - 2.11.0-0.2.Beta2.1
- Initial spec file heavily based on spec file by Steve Jenkins for
- Fedora/EPEL 2.11.0.Alpha0
