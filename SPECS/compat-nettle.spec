%define _prefix /opt/gnutls

Name:           compat-nettle
Version:        3.4.1
Release:        1%{?dist}
Summary:        A low-level cryptographic library

Group:          Development/Libraries
License:        GPLv2 GPLv3 LGPLv3
URL:            http://www.lysator.liu.se/~nisse/nettle/
Source0:        http://www.lysator.liu.se/~nisse/archive/nettle-%{version}.tar.gz

BuildRequires:  gmp-devel m4 texlive-dvips ghostscript
BuildRequires:  fipscheck
BuildRequires:	libtool, automake, autoconf

Requires(post): gnutls-slashopt
Requires(postun): gnutls-slashopt


%package devel
Summary:        Development headers for a low-level cryptographic library
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}
Requires:       gmp-devel%{?_isa}

%description
This build uses a prefix of %{_prefix} as a compatibility package. It is
not intended to replace the vendor version of this package.

Nettle is a cryptographic library that is designed to fit easily in more
or less any context: In crypto toolkits for object-oriented languages
(C++, Python, Pike, ...), in applications like LSH or GNUPG, or even in
kernel space.

This build uses an install prefix of %{_prefix} to avoid conflicts with
the version of nettle that ships with CentOS 7.

%description devel
This build uses a prefix of %{_prefix} as a compatibility package. It is
not intended to replace the vendor version of this package.

Nettle is a cryptographic library that is designed to fit easily in more
or less any context: In crypto toolkits for object-oriented languages
(C++, Python, Pike, ...), in applications like LSH or GNUPG, or even in
kernel space.  This package contains the files needed for developing 
applications with nettle.

This build uses an install prefix of %{_prefix} to avoid conflicts with
the version of nettle that ships with CentOS 7.


%prep
%setup -q -n nettle-%{version}
# Disable -ggdb3 which makes debugedit unhappy
#sed s/ggdb3/g/ -i configure

%build
%configure CC=c99 --enable-shared
make %{?_smp_mflags}

%define __spec_install_post \
	%{?__debug_package:%{__debug_install_post}} \
	%{__arch_install_post} \
	%{__os_install_post} \
	fipshmac -d $RPM_BUILD_ROOT%{_libdir} $RPM_BUILD_ROOT%{_libdir}/libnettle.so.6.* \
	fipshmac -d $RPM_BUILD_ROOT%{_libdir} $RPM_BUILD_ROOT%{_libdir}/libhogweed.so.4.* \
	file=`basename $RPM_BUILD_ROOT%{_libdir}/libnettle.so.6.*.hmac` && mv $RPM_BUILD_ROOT%{_libdir}/$file $RPM_BUILD_ROOT%{_libdir}/.$file && ln -s .$file $RPM_BUILD_ROOT%{_libdir}/.libnettle.so.6.hmac \
	file=`basename $RPM_BUILD_ROOT%{_libdir}/libhogweed.so.4.*.hmac` && mv $RPM_BUILD_ROOT%{_libdir}/$file $RPM_BUILD_ROOT%{_libdir}/.$file && ln -s .$file $RPM_BUILD_ROOT%{_libdir}/.libhogweed.so.4.hmac \
%{nil}


%install
make install DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"
make install-shared DESTDIR=$RPM_BUILD_ROOT INSTALL="install -p"
rm -f $RPM_BUILD_ROOT%{_libdir}/*.a
rm -rf $RPM_BUILD_ROOT%{_infodir}
chmod 0755 $RPM_BUILD_ROOT%{_libdir}/libnettle.so.6.*
chmod 0755 $RPM_BUILD_ROOT%{_libdir}/libhogweed.so.4.*
%check
make check

%files
%doc AUTHORS ChangeLog NEWS README TODO
%license COPYING*
%{_bindir}/nettle-lfib-stream
%{_bindir}/pkcs1-conv
%{_bindir}/sexp-conv
%{_bindir}/nettle-hash
%{_bindir}/nettle-pbkdf2
%{_libdir}/libnettle.so.6
%{_libdir}/libnettle.so.6.*
%{_libdir}/libhogweed.so.4
%{_libdir}/libhogweed.so.4.*
%{_libdir}/.libhogweed.so.*.hmac
%{_libdir}/.libnettle.so.*.hmac


%files devel
%doc descore.README nettle.html nettle.pdf
%license COPYING*
%{_includedir}/nettle
%{_libdir}/libnettle.so
%{_libdir}/libhogweed.so
%{_libdir}/pkgconfig/hogweed.pc
%{_libdir}/pkgconfig/nettle.pc

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig



%changelog
* Mon Dec 10 2018 Alice Wonder <buildmaster@librelamp.com> - 3.4.1-1
- Update to 3.4.1, do not install the info files.
- Build as compatibility package with /opt/gnutls as install prefix.

* Mon Aug  8 2016 Nikos Mavrogiannopoulos <nmav@redhat.com> - 2.7.1-8
- Use a cache-silent version of mpz_powm to prevent cache-timing
  attacks against RSA and DSA in shared VMs. (#1364897,CVE-2016-6489)

* Wed Mar  2 2016 Nikos Mavrogiannopoulos <nmav@redhat.com> - 2.7.1-5
- Fixed SHA-3 implementation to conform to final standard (#1252936)
- Fixed CVE-2015-8803 CVE-2015-8804 CVE-2015-8805 which caused issues
  in secp256r1 and secp384r1 calculations (#1314374)

* Tue Jul 29 2014 Nikos Mavrogiannopoulos <nmav@redhat.com> - 2.7.1-4
- Correct path of links (#1117782)

* Mon Jul 28 2014 Nikos Mavrogiannopoulos <nmav@redhat.com> - 2.7.1-3
- Added fipshmac checksum (#1117782)

* Fri Jan 24 2014 Daniel Mach <dmach@redhat.com> - 2.7.1-2
- Mass rebuild 2014-01-24

* Wed Jan 15 2014 Tomáš Mráz <tmraz@redhat.com> - 2.7.1-1
- Updated to nettle 2.7.1

* Fri Dec 27 2013 Daniel Mach <dmach@redhat.com> - 2.6-4
- Mass rebuild 2013-12-27

* Fri Dec 13 2013 Nikos Mavrogiannopoulos <nmav@redhat.com> - 2.6-3
- Added patch nettle-tmpalloc.patch (#1033570)

* Wed Feb  6 2013 Tomáš Mráz <tmraz@redhat.com> - 2.6-2
- nettle includes use gmp.h

* Tue Feb  5 2013 Tomáš Mráz <tmraz@redhat.com> - 2.6-1
- New upstream release

* Fri Jul 20 2012 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 2.4-4
- Rebuilt for https://fedoraproject.org/wiki/Fedora_18_Mass_Rebuild

* Thu Jul 05 2012 David Woodhouse <dwmw2@infradead.org> - 2.4-3
- Remove explicit buildroot handling and defattr.

* Wed Jul 04 2012 David Woodhouse <dwmw2@infradead.org> - 2.4-2
- Review feedback

* Mon Jun 18 2012 David Woodhouse <dwmw2@infradead.org> - 2.4-1
- Revive package (GnuTLS needs it), disable static, update to current release 2.4

* Sat Jul 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.15-7
- Rebuilt for https://fedoraproject.org/wiki/Fedora_12_Mass_Rebuild

* Wed Feb 25 2009 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.15-6
- Rebuilt for https://fedoraproject.org/wiki/Fedora_11_Mass_Rebuild

* Thu Apr 10 2008 Ian Weller <ianweller@gmail.com> 1.15-5
- Moved static lib to -static

* Mon Mar 24 2008 Ian Weller <ianweller@gmail.com> 1.15-4
- Added libraries and ldconfig

* Mon Feb 18 2008 Ian Weller <ianweller@gmail.com> 1.15-3
- Added provides -static to -devel

* Sun Feb 17 2008 Ian Weller <ianweller@gmail.com> 1.15-2
- Removed redundant requires
- Removed redundant documentation between packages
- Fixed license tag
- Fixed -devel description
- Added the static library back to -devel
- Added make clean

* Fri Feb 08 2008 Ian Weller <ianweller@gmail.com> 1.15-1
- First package build.
