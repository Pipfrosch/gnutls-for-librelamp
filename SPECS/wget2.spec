%define codate 20181221

Name:		wget2
Version:	0.%{codate}
Release:	1%{?dist}.1
Summary:	Wget2 is a fast multithreaded metalink/file/website downloader

Group:		Experimental/Non-Production
License:	GPLv3+ LGPLv3+
URL:		https://gitlab.com/gnuwget/wget2
Source0:	wget2-%{codate}.tar.xz
Source100:	clonewget2.sh
Source500:	wget2-%{codate}.tar.xz.sha256
# autotools
BuildRequires:	autoconf autogen automake gettext-devel libtool
BuildRequires:	gettext-devel >= 0.18.2
# end autotools
BuildRequires:	pkgconfig >= 0.28
BuildRequires:	gnulib-devel >= 0-27.20180720git
BuildRequires:	doxygen
BuildRequires:	pandoc
#compression libs
BuildRequires:	zlib-devel >= 1.2.3
BuildRequires:	lzip
BuildRequires:	bzip2-devel >= 1.0.6
BuildRequires:	brotli-devel
BuildRequires:	xz-devel
BuildRequires:	libzstd-devel
#tls - TODO let macro decide which
BuildRequires:	compat-gnutls-devel >= 3.3.0
#BuildRequires:	compat-nettle-devel
#BuildRequires:	p11-kit-devel
# other build dependencies
BuildRequires:	libidn2-devel >= 0.14
BuildRequires:	flex >= 2.5.35
BuildRequires:	compat-libpsl >= 0.5.0
BuildRequires:	libnghttp2-devel >= 1.34.0
BuildRequires:	pcre2-devel
BuildRequires:	compat-libpsl-devel
#BuildRequires:	libhsts
# for test suite
#BuildRequires:	libmicrohttpd >= 0.9.51
# optional
#BuildRequires:	lcov
#Requires:
Requires:	%{name}-libs%{?_isa} = %{version}-%{release}	

%description
This is an experimental release of a project that has not been finished. Some
features are missing and it may not be stable. Do not use for production
purposes.

GNU Wget2 is the successor of GNU Wget, a file and recursive website downloader.

Designed and written from scratch it wraps around libwget, that provides the basic
functions needed by a web client.

Wget2 works multi-threaded and uses many features to allow fast operation.

In many cases Wget2 downloads much faster than Wget1.x due to HTTP2, HTTP compression,
parallel connections and use of If-Modified-Since HTTP header.

GNU Wget2 is licensed under GPLv3+.

Libwget is licensed under LGPLv3+.

%package libs
Group:		System Environment/Libraries
License:        LGPLv3+
Summary:	The wget2 shared library
Requires(post):	/sbin/ldconfig
Requires(postun):	/sbin/ldconfig

%description libs
This is an experimental release of a project that has not been finished. Some
features are missing and it may not be stable. Do not use for production
purposes.

This package contains the libwget shared library.

%post libs -p /sbin/ldconfig

%postun libs -p /sbin/ldconfig

%package devel
Group:		Development/Libraries
License:        GPLv3+ LGPLv3+
Summary:	Development files for the wget2 shared library
Requires:	%{name}-libs%{?_isa} = %{version}-%{release}

%description devel
This is an experimental release of a project that has not been finished. Some
features are missing and it may not be stable. Do not use for production
purposes.

This package contains the header files, pkgconfig file, and man3 pages needed
to build and develop software that uses the libwget library.

%package static
Group:		Development/Libraries
License:        LGPLv3+
Summary:	The libwget static library
Requires:	%{name}-devel%{?_isa} = %{version}-%{release}

%description static
This is an experimental release of a project that has not been finished. Some
features are missing and it may not be stable. Do not use for production
purposes.

This package containst the libwget.a static library.


%prep
# After tarballs released, switch to GnuPG sig verify
( cd %_sourcedir; sha256sum -c %{SOURCE500} )
%setup -q -n wget2-%{codate}
# version of gnulib being used does not have these
#sed -i 's|gnulib_tool=$GNULIB_SRCDIR/gnulib-tool.py|gnulib_tool=/usr/bin/gnulib-tool|' bootstrap
#sed -i 's|doc/INSTALL||' bootstrap


%build
export GNULIB_TOOL=%{_bindir}/gnulib-tool
export GNULIB_SRCDIR=%{_datadir}/gnulib
#./bootstrap --gnulib-srcdir=%%{_datadir}/gnulib --no-git --skip-po --no-bootstrap-sync
./bootstrap --no-git --skip-po --no-bootstrap-sync
export PKG_CONFIG_PATH="/opt/gnutls/%{_lib}/pkgconfig"
%configure
make %{?_smp_mflags}


%install
make install DESTDIR=%{buildroot}
rm -f %{buildroot}%{_libdir}/*.la


#%%check
#%make check %{?_smp_mflags}


%files
%defattr(-,root,root,-)
%doc AUTHORS NEWS todo.txt README.md
%license COPYING
%{_bindir}/wget2
%{_bindir}/wget2_noinstall
%{_mandir}/man1/wget2.1*

%files libs
%defattr(-,root,root,-)
%license COPYING.LESSER
%{_libdir}/libwget.so.*

%files devel
%defattr(-,root,root,-)
%license COPYING COPYING.LESSER
%{_includedir}/*.h
%{_libdir}/libwget.so
%{_libdir}/pkgconfig/libwget.pc
%{_mandir}/man3/*.3*

%files static
%defattr(-,root,root,-)
%license COPYING.LESSER
%{_libdir}/libwget.a



%changelog
* Fri Dec 21 2018 Alice Wonder <buildmaster@librelamp.com> - 0.20181221-1.1
- Initial build from gitlab master checkout
- PACKAGING TODO
-   BUILDSTDERR: configure: WARNING: *** libhsts was not found.
-   BUILDSTDERR: configure: WARNING: *** GPGME not found. Signature verification is not possible
-   BUILDSTDERR: checking for MHD_free... configure: WARNING: *** LIBMICROHTTPD was not found.
