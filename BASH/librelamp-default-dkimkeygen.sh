#!/bin/bash

# Written by Michael A. Peters for Pipfrosch Press.
# Due to the straight-forward obvious simplistic nature of this shell script
# it is released under a CC0 license (effectively Public Domaim).
#
# Monday Dec 31, 2018

if [ "$(id -u)" != "0" ]; then
  echo "Please run this script as root."
  exit 1
fi
if [ "x`grep -c "^opendkim" /etc/group`" == "x0" ]; then
  echo "The group opendkim does not exist. Exiting now."
  exit 1
fi

KEYGEN=/usr/sbin/dkim-keygen
SELECTOR="default`date +%Y`"
# detect hostname
which postcofnf > /dev/null 2>&1
if [ $? -eq 0 ]; then
  DOMAIN="`postconf |grep "^mydomain =" |cut -d"=" -f2 |sed -e s?" "?""?`"
else
  DOMAIN="`hostname --domain`"
  if [ "x${DOMAIN}" == "x" ]; then
    DOMAIN="`hostname`"
  fi
fi

# make sure to use IDNA for non-ASCII domain names
DOMAIN="`${KEYGEN} -d "${DOMAIN}" --clean-domain`"
if [ $? -ne 0 ]; then
  DOMAIN="localhost.localdomain"
fi

KEYDIR="/etc/opendkim/keys/${DOMAIN}"
PRIVATE="${KEYDIR}/${SELECTOR}.private"
DNS="${KEYDIR}/${SELECTOR}.dns.txt"

if [ ! -d ${KEYDIR} ]; then
  mkdir -p ${KEYDIR}
  chown root:opendkim ${KEYDIR}
  chmod 770 ${KEYDIR}
fi

if [ ! -d ${KEYDIR} ]; then
  echo "Could not successfully create ${KEYDIR}"
  echo "exiting now."
  exit 1
fi

${KEYGEN} -D ${KEYDIR} -d ${DOMAIN} -s ${SELECTOR} -v
if [ $? -ne 0 ]; then
  exit 1
fi

if [ ! -f ${PRIVATE} ]; then
  echo "Unexpected Error: Private key not found at ${PRIVATE}"
  exit 1
fi
chown root:opendkim ${PRIVATE}
chmod 0440 ${PRIVATE}
[ -f ${DNS} ] && cat ${DNS}

exit 0

