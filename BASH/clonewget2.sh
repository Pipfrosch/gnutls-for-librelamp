#!/bin/bash
# clonewget2.sh

DIR=`mktemp -d /tmp/wget2.XXXXXXXX`
TODAY=`date +%Y%m%d`
pushd ${DIR}
git clone https://gitlab.com/gnuwget/wget2.git
mv wget2 wget2-${TODAY}
tar -Jcf wget2-${TODAY}.tar.xz wget2-${TODAY}
sha256sum wget2-${TODAY}.tar.xz > wget2-${TODAY}.tar.xz.sha256
cp wget2-${TODAY}.* ~/rpmbuild/SOURCES/
echo "update spec file codate macro to ${TODAY}"
rm -f wget2-${TODAY}.*
popd
